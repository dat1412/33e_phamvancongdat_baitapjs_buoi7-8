// Lấy số từ ô input push vào mảng
var numberArr = [];

function themSoTuInput() {
  if (document.getElementById("txt-number").value.trim() == "") {
    return;
  }

  var numberValue = document.getElementById("txt-number").value * 1;
  numberArr.push(numberValue);

  document.getElementById("txt-number").value = "";

  document.getElementById("listNumber").innerHTML = `${numberArr}`;
}

console.log("numberArr: ", numberArr);

// Bải tập 1: Tính tổng số dương trong mảng
function tinhTongSoDuong() {
  var tongSoDuong = 0;
  numberArr.forEach(function (num) {
    if (num > 0) {
      tongSoDuong += num;
    }
  });

  document.getElementById(
    "result-ex-1"
  ).innerHTML = `Tổng số dương trong mảng là: ${tongSoDuong}`;
}

// Bài tập 2: Đếm số dương trong mảng
function demSoDuong() {
  var count = 0;
  numberArr.forEach(function (num) {
    if (num > 0) {
      count++;
    }
  });

  document.getElementById(
    "result-ex-2"
  ).innerHTML = `Có ${count} số dương trong mảng`;
}

// Bài Tập 3: Tìm số nhỏ nhất trong mảng
function timSoNhoNhat() {
  var minNumber = numberArr[0];
  numberArr.forEach(function (num) {
    if (minNumber > num) {
      minNumber = num;
    }
  });

  document.getElementById(
    "result-ex-3"
  ).innerHTML = ` Số nhỏ nhất trong mảng là: ${minNumber}`;
}

// Bài tập 4: Tìm số dương nhỏ nhất trong mảng
function timSoDuongNhoNhat() {
  var soDuongArr = [];
  numberArr.forEach(function (num) {
    if (num > 0) {
      soDuongArr.push(num);
    }
  });

  if (soDuongArr.length > 0) {
    var soDuongNhoNhat = soDuongArr[0];
    soDuongArr.forEach(function (num) {
      if (soDuongNhoNhat > num) {
        soDuongNhoNhat = num;
      }
    });
    document.getElementById(
      "result-ex-4"
    ).innerHTML = `Số dương nhỏ nhất trong mảng là: ${soDuongNhoNhat}`;
  } else {
    document.getElementById(
      "result-ex-4"
    ).innerHTML = `Không có số dương nào trong mảng`;
  }
}

// Bài tập 5: Tìm số chắn cuối cùng trong mảng
function timSoChanCuoiCung() {
  var soChanArr = [];
  numberArr.forEach(function (num) {
    if (num % 2 == 0) {
      soChanArr.push(num);
    }
  });

  if (soChanArr.length > 0) {
    var soChanCuoiCung = soChanArr[soChanArr.length - 1];
    document.getElementById(
      "result-ex-5"
    ).innerHTML = ` Số chẵn cuối cùng trong mảng là ${soChanCuoiCung}`;
  } else {
    document.getElementById(
      "result-ex-5"
    ).innerHTML = `Không có số chẵn nào trong mảng`;
  }
}

// Bài tập 6: Đổi chỗ 2 vị trí đc chọn

function doiViTri() {
  var index_1 = document.getElementById("index-1").value * 1;
  var index_2 = document.getElementById("index-2").value * 1;

  if (
    index_1 > 0 &&
    index_2 > 0 &&
    index_1 <= numberArr.length &&
    index_2 <= numberArr.length
  ) {
    var valueArr = numberArr[index_1];
    numberArr[index_1] = numberArr[index_2];
    numberArr[index_2] = valueArr;

    document.getElementById(
      "result-ex-6"
    ).innerHTML = `Mảng sau khi đổi: ${numberArr}`;
  } else {
    document.getElementById(
      "result-ex-6"
    ).innerHTML = `Ví trí muốn đổi phải lớn hơn 0 và nhỏ thua độ dài mảng`;
  }
}

// Bài 7: Sắp xếp mảng theo thứ tự tăng dần

function sapXep() {
  var newArr = numberArr.sort(function (a, b) {
    return a - b;
  });

  document.getElementById(
    "result-ex-7"
  ).innerHTML = `Mảng sau khi sắp xếp theo thứ tự tăng dần là: ${newArr}`;
}

// Bài 8: Tìm số nguyên tố đầu tiên trong mảng

function timSoNguyenToDauTien() {
  var soNguyenToDauTien;

  function kiemTraSoNguyenTo(n) {
    var checkSNT = true;
    if (n < 2) {
      checkSNT = false;
    } else if (n == 2) {
      checkSNT = true;
    } else if (n % 2 == 0) {
      checkSNT = false;
    } else {
      for (var i = 3; i < n - 1; i += 2) {
        if (n % i == 0) {
          checkSNT = false;
          break;
        }
      }
    }
    return checkSNT;
  }

  for (var index = 0; index < numberArr.length; index++) {
    if (kiemTraSoNguyenTo(numberArr[index]) == true) {
      soNguyenToDauTien = numberArr[index];
      document.getElementById(
        "result-ex-8"
      ).innerHTML = `Số nguyên tố đầu tiên trong mảng là: ${soNguyenToDauTien}`;
      break;
    } else {
      document.getElementById(
        "result-ex-8"
      ).innerHTML = `Trong mảng không có số nguyên tố nào`;
    }
  }
}

// Bài 9: Nhập thêm 1 mảng số thực, tìm xem trong mảng có bao nhiêu số nguyên

var numberNewArr = [];
function themSoTuInputNewArr() {
  if (document.getElementById("txt-new-number").value.trim() == "") {
    return;
  }

  var newNumberValue = document.getElementById("txt-new-number").value * 1;
  numberNewArr.push(newNumberValue);

  document.getElementById("txt-new-number").value = "";

  document.getElementById("newListNumber").innerHTML = `${numberNewArr}`;
}

function demSoNguyen() {
  var demSoNguyen = 0;
  numberNewArr.forEach(function (num) {
    if (Number.isInteger(num) == true) {
      demSoNguyen++;
    }
  });

  document.getElementById(
    "result-ex-9"
  ).innerHTML = ` Số lượng số nguyên trong mảng mới là: ${demSoNguyen}`;
}

// Bài 10: So sánh số lượng số âm, số dương
function soSanh() {
  var soLuongSoDuong = 0;
  var soLuongSoAm = 0;
  numberArr.forEach(function (num) {
    if (num < 0) {
      soLuongSoAm++;
    }
    if (num > 0) {
      soLuongSoDuong++;
    }
  });

  var resultEx10 = document.getElementById("result-ex-10");

  if (soLuongSoDuong > soLuongSoAm) {
    resultEx10.innerHTML = ` Số dương > số âm`;
  } else if (soLuongSoDuong < soLuongSoAm) {
    resultEx10.innerHTML = ` Số dương < số âm`;
  } else {
    resultEx10.innerHTML = ` Số dương = số âm`;
  }
}
